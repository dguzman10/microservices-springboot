# MicroServices using SpringBoot & SpringCloud #


### Monoliths ###
A single deployment unit (EAR or WAR).
Code highly depends (cohesive code) on other parts of the code

Cons:
- Large codebases become mess over the time
- Multiple teams working on single codebase become tedious
- It is not possible to scale up only certain parts of the application
- Technology updates/rewrites become complex and expensive tasks

Pros:
Easy to deploy and monitor

### MicroServices ###
One of many services, independently deployable, working and communicating together to make up an application

Cons:
- Getting correct sub-domain boundaries, in the beginning, is hard
- Need more skilled developers to handle distributed application complexities
- Managing MicroServices based applications without proper DevOps culture is next to impossible
- Local developer environment setup might become complex to test cross-service communications.
	Though using Docker/Kubernetes this can be mitigated to some extent.

Pros: 
- Comprehending smaller codebase is easy
- Can independently scale up highly used services
- Each team can focus on one (or few) MicroService(s)
- Technology updates/rewrites become simpler


### SpringBoot ###

The most popular and widely used Java framework for building MicroServices. Many organizations are deploying applications
in a cloud environment. SpringCloud can be used with SpringBoot to build these Cloud native applications.


### Spring Cloud ###
An implementation of various design patterns to be followed while building Cloud Native applications. 
Provides Spring Cloud modules to address any application concerns.

# Modules: #

# Spring Cloud Config Server #
Externalizes configuration of applications in a central config server with the ability to update 
the configuration values without requiring to restart the applications.

Can be used with Git, Consul or ZooKeeper as config repository to store configuration parameters.

Configures the location of the Spring Cloud Config server in a microservice so that it will load all the properties when the application is ran.

Consul is a distributed service mesh to connect, secure, and configure services across any runtime platform and public or private cloud. 
Apache ZooKeeper is an effort to develop and maintain an open-source server which enables highly reliable distributed coordination.

Sensitive data like database credentials, keys, tokens etc. we don’t want to store them in plain text. 
Instead, store them in an encrypted format. Spring Cloud Config Server provides the ability to encrypt and decrypt the data.

Spring Cloud also provides the integration with *Vault* , a data storage tool, that stores any sensitive configuration properties.

How to create a Spring Cloud Server?
- Configure the location of git repository where we are going to store all our configuration files in the application.properties file.
- Now annotate the entry point class with @EnableConfigServer.
- and then you just need to add application-specific config files in the git repository

# Service Registry and Discovery #
A mechanism so that service-to-service communication should not depend on hard-coded hostnames and port numbers

Provides Netflix Eureka-based Service Registry and Discovery support with minimal configuration.

The Netflix Eureka Server creates a Service Registry and makes microservices as Eureka Clients so that as soon as we start a microservice 
it will get registered with Eureka Server automatically with a logical Service ID. Then, the other microservices, which are also Eureka Clients, can use Service ID to invoke REST endpoints.

Spring Cloud makes it very easy to create a Service Registry and discovering other services using Load Balanced RestTemplate.

Uses Consul or ZooKeeper for Service Registry and Discovery.

How to Create a Service Registry using Netflix Eureka Based Service:

- Add Eureka Server starter
- Then, add @EnableEurekaServer annotation to make our SpringBoot application a Eureka Server based Service Registry

By default, each Eureka Server is also a Eureka client and needs at least one service URL to locate a peer.
You can configure the port you want the eureka server running on and you can go to that port to see the Spring Eureka UI

When a service is registered with Eureka Server it keeps sending heartbeats for certain interval. If Eureka server didn’t receive heartbeats 
from any service instance it will assume service instance is down and take it out from the pool.

How to Register Microservices as Eureka Clients:

- Add the Eureka Discovery starter
- cofigure eureka.client.service-url.defaultZone propert in applications.properties to automatically register with the Eureka Server.

In the scenario of 2 instances of your application running:
We can register RestTemplate as a Spring bean with @LoadBalanced annotation. The RestTemplate with @LoadBalanced annotation will internally 
use Ribbon LoadBalancer to resolve the ServiceID and invoke REST endpoint using one of the available servers.

Uses Netflix Hystrix


# Circuit Breaker #
In microservices based architecture, one service might depend on another service and if one service goes down then failures may cascade to other services as well. 
Spring Cloud provides Netflix Hystrix based Circuit Breaker to handle these kinds of issues.

Implementing Netflix Hystrix Circuit Breaker:

In the case of a service being down or taking too long to respond which results in slowing down all the services depending on it,
This results in a timeout in which you must implement some fallback mechanism. 

- Add Hystrix starter
- Add @EnableCircuitBreaker to entry-point class / main 
- Use @HystrixCommand annotation on any method we want to apply timeout and fallback method.

Spring Cloud also provides a nice dashboard to monitor the status of Hystrix commands

# Spring Cloud Data Streams #
provides higher-level abstractions to use those frameworks in an easier manner.

# Spring Cloud Security #
Accessing microservices with authentication (usually a Single Sign-On feature that communicates across services)
Provides authentication services using OAuth2



# Distributed Tracing #
An issue with microservices is the ability to debug issues. There should be a mechanism to trace a chain of microservice calls.
You can use Spring Clouth Sleuth with Zipkin to trace the cross-service invocations

Spring Cloud Sleuth provides the distributed tracing capabilities and we can also export these trace information to Zipkin to visualize the call traces.

- Add Sleuth starter

Adds logs to your services that traces information 
You can then export this information to Zipkin

# Spring Cloud Contract #
There is a high chance that separate teams work on different microservices. There should be a mechanism for teams to agree upon API endpoint contracts so that each team can develop their APIs independently.
Helps to create such contracts and validate them by both service provider and consumer.

# Spring Cloud Zuul Proxy as API Gateway #
Provides a unified proxy interface that will delegate the calls to various microservices based on URL pattern

Zuul Proxy Server is a gateway application that handles all the requests and does the dynamic routing of microservice applications

Why do we need an API Gateway?
To provide a unified interface for a set of microservices so clients don't know about all the details of microservices internals.

Cons:
- It could become a single point of failure if proper measures are not taken to make it highly available
- Knowledge of various microservice API may creep into API Gateway

Pros:
- Provides easier interface to clients
- Can be used to prevent exposing the internal microservices structure to clients
- Allows to refactor microservices without forcing the clients to refactor consuming logic
- Can centralize cross-cutting concerns like security, monitoring, rate limiting etc

Uses the Zuul dependency
Adds @EnableZuulProxy annotation in the main entry-point class



